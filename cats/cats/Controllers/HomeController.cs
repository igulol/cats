﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace cats.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        /// <summary>
        /// Хороший результат
        /// </summary>
        /// <returns></returns>
        public ActionResult GoodResult()
        {
            ViewBag.Title = "GoodResult";

            return View();
        }

        /// <summary>
        /// Ожидание
        /// </summary>
        /// <returns></returns>
        public ActionResult WaitResult()
        {
            Thread.Sleep(5000);
            return View();
        }

        /// <summary>
        /// Ошибка
        /// </summary>
        /// <returns></returns>
        public ActionResult ErrorResult()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
