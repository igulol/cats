﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cats;
using cats.Controllers;
using System;

namespace cats.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Упорядочение
            HomeController controller = new HomeController();

            // Действие
            ViewResult result = controller.Index() as ViewResult;

            // Утверждение
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }

        [TestMethod]
        public void GoodResult()
        {
            // Упорядочение
            HomeController controller = new HomeController();

            // Действие
            ViewResult result = controller.GoodResult() as ViewResult;

            // Утверждение
            Assert.IsNotNull(result);
            //Assert.AreEqual("GoodResult", result.ViewBag.Title);
            Random rand = new Random();

            int rnd = rand.Next(0, 2); 

            Assert.AreEqual(rnd, 1);
        }
    }
}
